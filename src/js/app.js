document.addEventListener('DOMContentLoaded', function() {

    event_listeners();
    dark_mode();

})

function event_listeners() {
    const mobile_menu = document.querySelector('.mobile-menu');

    mobile_menu.addEventListener('click', hamburger_navigation);
}

function hamburger_navigation() {
    //console.log('Hamburger menu');
    const nav_area = document.querySelector('.nav-area');

    nav_area.classList.toggle('show');    // This will add the 'show' class to nav_area if it does not have it or remove it if it has it.
    // This replaces the following code
    /* if(navigation.classList.contains('show')){
        navigation.classList.remove('show');
    }
    else {
        navigation.classList.add('show');
    } */
}

function dark_mode() {
    // Check the browser-configured dark mode preference 
    // -----------------------------------------------------------------------------------------------------------------------------------
    const DarkModePreference = window.matchMedia('(prefers-color-scheme: dark)'); 

    //console.log(DarkModePreference.matches); // This will be true if the user selects the dark mode on the browser and false otherwise

    // When the site loads for the first time, add the dark-mode class or not according to the browsers mode configuration
    if(DarkModePreference.matches) {
        document.body.classList.add('dark-mode');
    }
    else {
        document.body.classList.remove('dark-mode');
    }

    // We can also change to dark mode without refreshing the page when the user changes the browser light/dark mode configuration
    DarkModePreference.addEventListener('change', function() {
        if(DarkModePreference.matches) {
            document.body.classList.add('dark-mode');
        }
        else {
            document.body.classList.remove('dark-mode');
        }
    })
    // -----------------------------------------------------------------------------------------------------------------------------------
    
    const dark_mode_button = document.querySelector('.dark-mode-button');   
    
    dark_mode_button.addEventListener('click', function() {
        document.body.classList.toggle('dark-mode');    // Add/remove 'dark-mode' class to <body> on click
    });
}


